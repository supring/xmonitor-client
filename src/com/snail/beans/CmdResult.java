/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.snail.beans;

import com.snail.utils.HTMLDecoder;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author pm
 */
public class CmdResult {

    private String output = "";

    private boolean success = false;

    /**
     * Get the value of success
     *
     * @return the value of success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Set the value of success
     *
     * @param success new value of success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Get the value of output
     *
     * @return the value of output
     */
    public String getOutput() {
        return output;
    }

    /**
     * Set the value of output
     *
     * @param output new value of output
     */
    public void setOutput(String output) {
        this.output = output;
    }
}
