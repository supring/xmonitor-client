/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.snail.task;

import com.snail.main.XTask;
import com.woniu.utils.sql.mysql.MySQL;
import it.sauronsoftware.cron4j.TaskExecutionContext;
import java.sql.SQLException;
import org.jsoup.nodes.Element;

/**
 *
 * @author pm
 */
public class MySQLTask extends XTask {

    public MySQLTask(Element full_cfg, Element cfg, Element module) {
        super(full_cfg, cfg, module);
    }

    @Override
    public void onExecute(TaskExecutionContext tec) throws RuntimeException {
        MySQL mysql = new MySQL(getText(cfg.select(">server>host").first()),
                getText(cfg.select(">server>port").first()),
                getText(cfg.select(">server>dbname").first()),
                getText(cfg.select(">server>user").first()),
                getText(cfg.select(">server>pass").first()));
        try {
            mysql.getConnection();
            mysql.close();
            onSuccess("");
        } catch (SQLException e) {
            onError("2", e.getMessage());
        }
    }
 
}
